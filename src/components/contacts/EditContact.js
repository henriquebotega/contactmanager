import React, { Component } from 'react'
// import uuid from 'uuid'
import axios from 'axios'

import { Consumer } from '../../context'
import TextInputGroup from '../layout/TextInputGroup';
import { validarEmail, isObjectEmpty } from '../layout/util';

class EditContact extends Component {
    state = {
        id: '',
        name: '',
        email: '',
        phone: '',
        errors: {
            name: {},
            email: {},
            phone: {}
        }
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    async componentDidMount() {
        const res = await axios.get('https://jsonplaceholder.typicode.com/users/' + this.props.match.params.id)
        const contact = res.data

        this.setState({
            id: contact.id,
            name: contact.name,
            email: contact.email,
            phone: contact.phone
        })
    }

    saveContact = (e, dispatch) => {
        e.preventDefault()

        const { name, email, phone } = this.state

        this.setState({
            errors: {
                name: {},
                email: {},
                phone: {}
            }
        })

        if (name === '') {
            setTimeout(() => {
                this.setState({
                    errors: { ...this.state.errors, name: 'Name is required' }
                })
            }, 100)
        }

        if (email === '') {
            setTimeout(() => {
                this.setState({
                    errors: { ...this.state.errors, email: 'E-mail is required' }
                })
            }, 200)
        } else {
            if (!validarEmail(email)) {
                setTimeout(() => {
                    this.setState({
                        errors: { ...this.state.errors, email: 'E-mail is invalid format' }
                    })
                }, 300)
            }
        }

        if (phone === '') {
            setTimeout(() => {
                this.setState({
                    errors: { ...this.state.errors, phone: 'Phone is required' }
                })
            }, 400)
        }

        setTimeout(async () => {
            if (isObjectEmpty(this.state.errors.name) && isObjectEmpty(this.state.errors.email) && isObjectEmpty(this.state.errors.phone)) {

                const id = this.props.match.params.id

                const newContact = {
                    name, email, phone
                }

                const res = await axios.put('https://jsonplaceholder.typicode.com/users/' + id, newContact)

                dispatch({
                    type: "UPD_CONTACT",
                    payload: res.data
                })

                this.setState({
                    name: '',
                    email: '',
                    phone: '',
                    errors: {
                        name: {},
                        email: {},
                        phone: {}
                    }
                })

                this.props.history.push('/')
            }
        }, 500)
    }

    render() {
        const { name, email, phone, errors } = this.state

        return (
            <Consumer>
                {value => {
                    const { dispatch } = value

                    return (
                        <div className='card mb-3'>
                            <div className="card-header">Edit Contact</div>

                            <div className="card-body">
                                <form onSubmit={(e) => this.saveContact(e, dispatch)}>
                                    <TextInputGroup error={errors.name} label="Name" name="name" value={name} placeholder="Enter your name" onChange={this.handleChange} />
                                    <TextInputGroup type="email" error={errors.email} label="E-mail" name="email" value={email} placeholder="Enter your email" onChange={this.handleChange} />
                                    <TextInputGroup error={errors.phone} label="Phone" name="phone" value={phone} placeholder="Enter your phone" onChange={this.handleChange} />

                                    <input type="submit" value="Save" className="btn btn-light btn-block" />
                                </form>
                            </div>
                        </div>
                    )
                }}
            </Consumer>
        )
    }
}

export default EditContact