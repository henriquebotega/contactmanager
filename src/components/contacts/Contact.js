import React, { Component } from 'react'
import PropTypes from 'prop-types';
import axios from 'axios'
import { Link } from 'react-router-dom'

import { Consumer } from '../../context'

class Contact extends Component {

    state = {
        showContactInfo: false
    }

    deleteClick = async (id, dispatch) => {
        try {
            await axios.delete('https://jsonplaceholder.typicode.com/users/' + id)
        } catch (e) {
            console.log('Erro: deleteClick')
        } finally {
            dispatch({
                type: "DELETE_CONTACT",
                payload: id
            })
        }
    }

    showClick = () => {
        this.setState({
            showContactInfo: !this.state.showContactInfo
        })
    }

    render() {
        const { showContactInfo } = this.state
        const { contact } = this.props
        const { id, name, email, phone } = contact

        return (
            <Consumer>
                {value => {

                    const { dispatch } = value

                    return (
                        <div className="card card-body mb-3">
                            <h4>
                                {name} {' '}
                                <i onClick={this.showClick} className="fas fa-sort-down" style={{ cursor: 'pointer' }}></i>
                                <i onClick={() => this.deleteClick(contact.id, dispatch)} className="fas fa-times" style={{ cursor: 'pointer', float: 'right', color: 'red' }}></i>

                                <Link to={'contact/edit/' + id}>
                                    <i className="fa fa-pencil-alt" style={{ cursor: 'pointer', float: 'right', color: 'orange', marginRight: '1rem' }}></i>
                                </Link>
                            </h4>

                            {showContactInfo &&
                                <ul className="list-group">
                                    <li className="list-group-item">Email: {email}</li>
                                    <li className="list-group-item">Phone: {phone}</li>
                                </ul>
                            }
                        </div>
                    )
                }}
            </Consumer>
        )
    }
}

Contact.propTypes = {
    contact: PropTypes.object.isRequired
}

export default Contact