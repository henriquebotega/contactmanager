import React, { Component } from 'react'

class AddContact extends Component {

    constructor(props) {
        super(props)

        this.nameInput = React.createRef()
        this.emailInput = React.createRef()
        this.phoneInput = React.createRef()
    }

    saveContact = (e) => {
        e.preventDefault()

        const contact = {
            name: this.nameInput.current.value,
            email: this.emailInput.current.value,
            phone: this.phoneInput.current.value
        }

        console.log(contact)
    }

    static defaultProps = {
        name: 'Fred',
        email: 'fred@server.com',
        phone: '555-888-777'
    }

    render() {
        const { name, email, phone } = this.props

        return (
            <div className='card mb-3'>
                <div className="card-header">Add Contact</div>
                <div className="card-body">
                    <form>
                        <div className="form-group">
                            <label htmlFor="name">Name</label>
                            <input type="text" className="form-control form-control-lg" placeholder='Enter your name'
                                defaultValue={name} ref={this.nameInput} name='name' />
                        </div>
                        <div className="form-group">
                            <label htmlFor="email">E-mail</label>
                            <input type="email" className="form-control form-control-lg" placeholder='Enter your email'
                                defaultValue={email} ref={this.emailInput} name='email' />
                        </div>
                        <div className="form-group">
                            <label htmlFor="phone">Phone</label>
                            <input type="text" className="form-control form-control-lg" placeholder='Enter your phone'
                                defaultValue={phone} ref={this.phoneInput} name='phone' />
                        </div>

                        <input type="submit" value="Save" onClick={this.saveContact} className="btn btn-light btn-block" />
                    </form>
                </div>
            </div>
        )
    }
}

export default AddContact