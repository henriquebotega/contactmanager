export const isBlank = (vlr) => {
    return vlr === undefined || vlr === null || vlr === "" || vlr.length === 0 || isObjectEmpty(vlr) || vlr.toString().trim() === "";
}

export const validarEmail = (vlr) => {
    var parse_email = /^[a-z0-9.]+@[a-z0-9]+\.[a-z]+(\.[a-z]+)?$/i;
    return parse_email.test(vlr)
}

export const isObjectEmpty = (obj) => {
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop))
            return false;
    }

    return JSON.stringify(obj) === JSON.stringify({});
}