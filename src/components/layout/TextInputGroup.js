import React from 'react'
import PropTypes from 'prop-types'

import { isBlank } from './util'

const TextInputGroup = ({ label, name, value, placeholder, type, onChange, error }) => {
    return (
        <div className="form-group">
            <label htmlFor="name">{label}</label>

            <input type={type} className={(!isBlank(error) ? 'is-invalid' : '') + " form-control form-control-lg"} placeholder={placeholder}
                value={value} onChange={onChange} name={name} />

            {!isBlank(error) &&
                <div className="invalid-feedback">{error}</div>
            }
        </div>
    )
}

TextInputGroup.propTypes = {
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    placeholder: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    error: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    onChange: PropTypes.func.isRequired
}

TextInputGroup.defaultProps = {
    type: 'text'
}

export default TextInputGroup